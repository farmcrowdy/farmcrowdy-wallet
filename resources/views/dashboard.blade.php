<x-app-layout>
    <x-slot name="header">
        {{-- <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2> --}}
    </x-slot>

    {{-- <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    You're logged in!
                </div>
            </div>
        </div>
    </div> --}}
    <div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0 bg-gray-100">
      <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        <div class="flash-msg">
          @if (session('success'))
          <div class="text-green-500 text-sm">
            {{ session('success') }}
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          </div>
          @endif
          @if (session('error'))
          <div class="text-red-500 text-sm">
            @foreach (session('error') as $element)
              <p>{{ $element }}</p>
            @endforeach
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          </div>
          @endif
        </div>
        <form method="post" action="{{ route('create-account') }}">
          @csrf
        {{-- <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            Account ID
          </label>
          <input name="account_id" value="{{ old('account_id') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" required>
          @error('account_id')
          <span class="invalid-feedback" role="alert">
              <strong class="text-red-500 text-xs">{{ $message }}</strong>
          </span>
          @enderror
        </div> --}}

        <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            Account Name
          </label>
          <input name="account_name" value="{{ old('account_name') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" required>
          @error('account_name')
          <span class="invalid-feedback" role="alert">
              <strong class="text-red-500 text-xs">{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            Phone Number
          </label>
          <input name="phone_number" value="{{ old('phone_number') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" required>
          @error('phone_number')
          <span class="invalid-feedback" role="alert">
              <strong class="text-red-500 text-xs">{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
            Email
          </label>
          <input name="email" value="{{ old('email') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="email" required>
          @error('email')
          <span class="invalid-feedback" role="alert">
              <strong class="text-red-500 text-xs">{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            BVN
          </label>
          <input name="bvn" value="{{ old('bvn') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" required>
          @error('bvn')
          <span class="invalid-feedback" role="alert">
              <strong class="text-red-500 text-xs">{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            Platform
          </label>
          <input name="platform" value="{{ old('platform') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text">
          @error('platform')
          <span class="invalid-feedback" role="alert">
              <strong class="text-red-500 text-xs">{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="flex items-center justify-between">
          <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
            Create
          </button>
        </div>
      </form>
  </div>
  </div>
</x-app-layout>
