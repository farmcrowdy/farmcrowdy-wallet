<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AccountController;
use App\Http\Controllers\Api\TransferController;
use App\Http\Controllers\Api\TransactionController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/transactions/{account_id}', [TransactionController::class, 'get_transaction_for_acc']);
Route::get('/transactions/{account_id}/{reference}', [TransactionController::class, 'get_transaction_by_ref']);
Route::get('/transactions', [TransactionController::class, 'get_merchant_transactions']);
Route::post('/transfers/inboundtransfers', [TransferController::class, 'inward_interbank_transfer']);
Route::get('/transfers/banks', [TransferController::class, 'banks']);
Route::apiResources([
    'accounts' => AccountController::class,
    'transfers' => TransferController::class,
]);
