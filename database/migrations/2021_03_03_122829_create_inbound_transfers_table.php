<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInboundTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbound_transfers', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->nullable();
            $table->string('account_number')->nullable();
            $table->string('from_acct_name')->nullable();
            $table->string('from_account')->nullable();
            $table->string('amount')->nullable();
            $table->string('narration')->nullable();
            $table->string('fee')->nullable();
            $table->string('operation')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbound_transfers');
    }
}
