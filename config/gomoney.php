<?php

return [
  'secretkey' => env('GOMONEY_PRIVATE_KEY', ''),
  'nip_secretkey' => env('GOMONEY_NIP_SECRET_KEY', ''),
  'publickey' => env('GOMONEY_PUBLIC_KEY', ''),
  'base_url' => env('GOMONEY_BASE_URL', 'https://api.gomoney.global')
];
