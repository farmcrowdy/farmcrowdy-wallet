<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id', 'account_number', 'narration', 'banks_code', 'amount',
        'response_code', 'transaction_id', 'status'
    ];
}
