<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InboundTransfer extends Model
{
    use HasFactory;

    protected $fillable = [
        'reference', 'account_number', 'narration', 'from_acct_name',
        'amount', 'from_account', 'fee', 'operation', 'status'
    ];
}
