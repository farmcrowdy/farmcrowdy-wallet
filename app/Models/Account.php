<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id', 'account_name', 'phone_number', 'email',
        'account_number', 'balance', 'ledger_balance', 'platform'
    ];
}
