<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Account;



class AccountController extends Controller
{
    public function create(Request $request) {
      $this->validate($request, [
        'account_id' => 'required',
        'account_name' => 'required',
        'phone_number' => 'required',
        'email' => 'required|string|email|max:255|unique:users',
        'bvn' => 'required',
        'platform' => 'required',
      ]);

      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');

      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->post($base_url.'/merchant/account', [
               "account_id" => $request->account_id,
               "phone_number" => $request->phone_number,
               "account_name" => $request->account_name,
               "email" => $request->email,
               "bvn" => $request->bvn,
             ]);

       $data = json_decode($response);

       //dd($data);

       if($data->status == "success"){

         $createRecord = Account::create([
           'account_id' => $request->account_id,
           'account_name' => $data->data->name,
           'email' => $request->email,
           'platform' => $request->platform,
           'phone_number' => $request->phone_number,
           'account_number' => $data->data->account_number,
           'balance' => $data->data->balance,
           'ledger_balance' => $data->data->ledger_balance
         ]);

         return back()->with('success', 'Account created successfully');
         //return response()->json(['data' => $data]);
       }else{
         //dd((array) $data->data);
         return back()->with('error', (array) $data->data);
         //return response()->json(['data' => $data]);
       }
   }
}
