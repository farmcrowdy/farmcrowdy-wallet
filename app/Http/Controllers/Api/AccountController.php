<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Models\Account;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      //dd($api_key);
      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->get($base_url.'/merchant/account');

       $data = json_decode($response);

       return response()->json(['status' => 200, 'data' => $data], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $validator = Validator::make($request->all(), [
       'account_name' => 'required',
       'phone_number' => 'required',
       'email' => 'required',
       'bvn' => 'required',
       'platform' => 'required',
     ]);

       // Throw error if validation fails
     if ($validator->fails()) {
       return response()->json(['status' => 400, 'message' => $validator->errors()], 400);
     }

     $api_key = config('gomoney.secretkey', '');
     $base_url = config('gomoney.base_url', '');

     $strPlatform = Str::slug($request->platform, '-');
     $account_id = $strPlatform.'-'.Str::random(5);

     $response = Http::withToken($api_key)
      ->withHeaders(['Content-Type' => 'application/json'])
      ->post($base_url.'/merchant/account', [
              "account_id" => $account_id,
              "phone_number" => $request->phone_number,
              "account_name" => $request->account_name,
              "email" => $request->email,
              "bvn" => $request->bvn,
            ]);

      $data = json_decode($response);

      if($data->status == "success"){

        $createRecord = Account::create([
          'account_id' => $data->data->id,
          'account_name' => $data->data->name,
          'email' => $request->email,
          'platform' => $request->platform,
          'phone_number' => $request->phone_number,
          'account_number' => $data->data->account_number,
          'balance' => $data->data->balance,
          'ledger_balance' => $data->data->ledger_balance
        ]);

        return response()->json(['status' => 200, 'message' => "Account created successfully", 'data' => $createRecord], 200);
        //return response()->json(['data' => $data]);
      }else{
        return response()->json(['status' => 400, 'data' => $data], 400);
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->get($base_url.'/merchant/account/'.$id);

       $data = json_decode($response);

       return response()->json(['status' => 200, 'data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
