<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TransactionController extends Controller
{
    public function get_merchant_transactions()
    {
      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->get($base_url.'/merchant/transactions');

       $data = json_decode($response);

       return response()->json(['status' => 200, 'data' => $data], 200);
    }

    public function get_transaction_by_ref($account_id, $reference)
    {
      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->get($base_url.'/merchant/transactions/'.$account_id.'/'.$reference);

       $data = json_decode($response);

       return response()->json(['status' => 200, 'data' => $data], 200);
    }

    public function get_transaction_for_acc($account_id)
    {
      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->get($base_url.'/merchant/transactions/'.$account_id);

       $data = json_decode($response);

       return response()->json(['status' => 200, 'data' => $data], 200);
    }


}
