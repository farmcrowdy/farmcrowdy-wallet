<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

use App\Models\Transfer;
use App\Models\InboundTransfer;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return response()->json(['data' => 'yes']);
    }

    /**
     * Display a listing of banks.
     *
     * @return \Illuminate\Http\Response
     */
    public function banks()
    {
      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->get($base_url.'/merchant/transfer/banks');

       $data = json_decode($response);

       return response()->json(['status' => 200, 'data' => $data], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'account_id' => 'required',
        'account_number' => 'required',
        'narration' => 'required',
        'bank_code' => 'required',
        'amount' => 'required',
      ]);

        // Throw error if validation fails
      if ($validator->fails()) {
        return response()->json(['status' => 400, 'message' => $validator->errors()], 400);
      }

      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');

      $response = Http::withToken($api_key)
       ->withHeaders(['Content-Type' => 'application/json'])
       ->post($base_url.'/merchant/transfer', [
               "account_id" => $request->account_id,
               "account_number" => $request->account_number,
               "narration" => $request->narration,
               "bank_code" => $request->bank_code,
               "amount" => $request->amount,
             ]);

       $data = json_decode($response);

       if($data->status == "success"){

         $createRecord = Transfer::create([
           'account_id' => $request->account_id,
           'account_number' => $request->account_number,
           'narration' => $request->narration,
           'banks_code' => $request->bank_code,
           'amount' => $request->amount,
           'response_code' => $data->data->response_code,
           'transaction_id' => $data->data->transaction_id,
           'status' => $data->status,
         ]);

         return response()->json(['status' => 200, 'message' => "Transfer successful", 'data' => $createRecord], 200);

       }else{
         return response()->json(['status' => 400, 'data' => $data], 400);
       }
    }

    public function inward_interbank_transfer(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'reference' => 'required',
        'account_number' => 'required',
        'from_acct_name' => 'required',
        'from_account' => 'required',
        'amount' => 'required',
        'fee' => 'required',
        'operation' => 'required',
        'narration' => 'required'
      ]);

        // Throw error if validation fails
      if ($validator->fails()) {
        return response()->json(['status' => 400, 'message' => $validator->errors()], 400);
      }

      $api_key = config('gomoney.secretkey', '');
      $base_url = config('gomoney.base_url', '');
      $nip_secret_key = config('gomoney.nip_secretkey', '');

      $myhash = hash_hmac('sha512', json_encode($request->all()), $nip_secret_key);
      $hashInBase64 = base64_encode($myhash);
      //dd($myhash);

      $response = Http::withToken($api_key)
       ->withHeaders([
         'Content-Type' => 'application/json',
         'x-gomoney-signature' => $hashInBase64
       ])
       ->post($base_url.'/nip/inbound/transfer', [
               "reference" => $request->reference,
               "account" => $request->account_number,
               "from_acct_name" => $request->from_acct_name,
               "from_account" => $request->from_account,
               "amount" => $request->amount,
               "narration" => $request->narration,
               "fee" => $request->fee,
               "operation" => $request->operation,
             ]);

       $data = json_decode($response);

       if($data->status == "success"){

         $createRecord = InboundTransfer::create([
           'reference' => $request->reference,
           'account_number' => $request->account_number,
           'narration' => $request->narration,
           'from_acct_name' => $request->from_acct_name,
           'amount' => $request->amount,
           'from_account' => $request->from_account,
           'fee' => $request->fee,
           'operation' => $request->operation,
           'status' => $data->status
         ]);

         return response()->json(['status' => 200, 'data' => $data], 200);

       }else{
         return response()->json(['status' => 400, 'data' => $data], 400);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
